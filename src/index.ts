/*
* Task 1: Days to New Year
*/
function getDaysToNewYear(date: string|Date): number {
    var startDate = new Date();
    if (typeof date === 'string') {
        // 22.10.2021
        let components = date.split('.').map(Number);
        startDate = new Date(components[2], components[1] - 1, components[0]);
    } else if (date instanceof Date) {
        startDate = date;
    }

    const endDate = new Date(2022, 0, 1);
    var one_day=1000*60*60*24;
    return Math.round((endDate - startDate) / one_day);
}
 
/*
* Task 2: Last to first
*/
function lastToFirst(value:string) {
    if (value.length <= 1) {
      return value;
    }
    var mid_char = value.substring(1, value.length - 1);
    return (value.charAt(value.length - 1)) + mid_char + value.charAt(0);
}

/*
* Task 3: Group organization users
*/
function groupOrgUsers(users) {
    var employees = [];
    var contractors = [];
    users.forEach(element => {
        switch (element.type) {
            case "EMPLOYEE":
                employees.push(element);
                break;
            case "CONTRACTOR":
                contractors.push(element);
                break;
        }
    });

    return { "employees": employees, "contractors": contractors }
}


export {getDaysToNewYear, lastToFirst, groupOrgUsers};
